﻿using System;

namespace ComunicationLayer
{
    /// <summary>
    /// This class is using to send alarm message from cloud to rasberry PI
    /// </summary>
    [Serializable]
    public class ActualAlert
    {
        public int AlertId { get; set; }
        public double Value { get; set; }
        public string Symbol { get; set; }
        public int SymbolId { get; set; }
        public bool AboveValue { get; set; }
        public bool EnableLed { get; set; }
        public bool EnableSpeaker { get; set; }
    }
}

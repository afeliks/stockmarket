﻿using ComunicationLayer;
using CryptoCurrencyWebJob.CRUD;
using Microsoft.Azure.Devices;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.ServiceBus;
using Microsoft.ServiceBus.Messaging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace CryptoCurrencyWebJob
{
    public class Functions
    {

        private static ServiceClient client;
        private static List<Device> devices;

        /// <summary>
        /// Gets message from Rasberry PI 
        /// </summary>
        /// <param name="message">The message</param>
        public static void GetMessage([EventHubTrigger(Consts.EVENT_HUB_NAME)] EventData message)
        {
            try
            {
                var contentType = message.SystemProperties["content-type"]?.ToString();

                if (contentType == "alertToRemove")
                {
                    var alertsToRemove = ConvertToList<ActualAlert>(message.GetBodyStream());
                    var alerts = new Alerts();

                    alertsToRemove.ForEach(a =>
                    {
                        alerts.RemoveAlert(a.AlertId);
                    });

                    SendAllAlerts();
                    AlarmsProcessing();
                }
                else if (contentType == "alerttoAdd")
                {
                    var alertsToAdd = ConvertToList<ActualAlert>(message.GetBodyStream());
                    var alerts = new Alerts();

                    alertsToAdd.ForEach(a =>
                    {
                        alerts.AddAlert(a.Symbol, a.Value, a.AboveValue, a.EnableLed, a.EnableSpeaker);
                    });

                    SendAllAlerts();
                }
                else if (contentType == "sendMeAlerts")
                {
                    SendAllAlerts();
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine($"{nameof(GetMessage)} : {ex.Message}");
            }
        }

        /// <summary>
        /// Gets rate of crypto currency from service and saves it in db
        /// </summary>
        [NoAutomaticTrigger]
        public static async Task GetRate()
        {
            await GetAllDevices();
            GetRateAsync();            
            //to prevent close the method
            while (true) { }
        }

        /// <summary>
        /// Gets rate async
        /// </summary>
        private static async void GetRateAsync()
        {
            try
            {

                Console.WriteLine($"{nameof(GetRateAsync)} is running");
                var delay = new System.TimeSpan(0, 1, 0); // 1 min
                client = ServiceClient.CreateFromConnectionString(Consts.IOT_HUB_CONNECTION_STRING);

                while (true)
                {
                    var adapter = new CryptoCurrencyRateAdapter();
                    adapter.GetRatesAndSave();

                    await SendActualTrades();
                    await AlarmsProcessing();
                    await Task.Delay(delay);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"{nameof(GetRateAsync)}: {ex.Message}");
            }
        }

        /// <summary>
        /// Sends actual trades for active symbols
        /// </summary>
        private static async Task SendActualTrades()
        {
            foreach (var d in devices)
            {
                Console.WriteLine($"{nameof(SendActualTrades)} is running");

                var actualTrades = new Trades().GetActualTrades();
                var convertedActualTrades = ConvertToByteArray(actualTrades);


                var message = new Message(convertedActualTrades);
                //message.ContentType = "trade"; //occurs problem
                message.Properties.Add("type", "trade");
                await client.SendAsync(d.Id, message);
            }
        }

        /// <summary>
        /// Processes alarms and send message to Rasberry PI
        /// </summary>
        private static async Task AlarmsProcessing()
        {
            Console.WriteLine($"{nameof(AlarmsProcessing)} is running");
            var alerts = new Alerts();
            var actualAlerts = alerts.GetActualAlerts();;
            await SendAlerts(actualAlerts);
        }

        /// <summary>
        /// Sends alarms to Rasberry PI
        /// </summary>
        private static async Task SendAlerts(List<ActualAlert>actualAlerts)
        {
            foreach (var d in devices)
            {
                Console.WriteLine($"{nameof(SendAlerts)} is running");
                var convertedActualAlerts = ConvertToByteArray(actualAlerts);

                var message = new Message(convertedActualAlerts); //send stream is not longer supported
                message.Properties.Add("type", "alert");
                await client.SendAsync(d.Id, message);
            }
        }

        private static async Task SendAllAlerts()
        {
            foreach (var d in devices)
            {
                var allAlerts = new Alerts().GetAlerts();
                Console.WriteLine($"{nameof(SendAllAlerts)} is running");
                var convertedActualAlerts = ConvertToByteArray(allAlerts);

                var message = new Message(convertedActualAlerts); //send stream is not longer supported
                message.Properties.Add("type", "allAlert");
                await client.SendAsync(d.Id, message);
            }
        }

        /// <summary>
        /// Converts object to byte array
        private static byte[] ConvertToByteArray<T>(List<T> list)
        {
            var serializer = new XmlSerializer(typeof(List<T>));
            var mStream = new MemoryStream();
            serializer.Serialize(mStream, list);
            return mStream.ToArray();
        }

        private static List<T> ConvertToList<T>(Stream stream)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(List<T>));
            return (List<T>)serializer.Deserialize(stream);
        }

        private async static Task GetAllDevices()
        {
            var registryManager = RegistryManager.CreateFromConnectionString(Consts.IOT_HUB_CONNECTION_STRING);
#pragma warning disable CS0618 // Type or member is obsolete
            devices = (await registryManager.GetDevicesAsync(2)).ToList();
#pragma warning restore CS0618 // Type or member is obsolete
        }
    }
}

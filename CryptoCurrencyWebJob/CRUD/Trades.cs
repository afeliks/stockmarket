﻿using ComunicationLayer;
using DB;
using DB.Tables;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace CryptoCurrencyWebJob.CRUD
{
    public class Trades : BaseCRUD
    {
 
        public Trades() : base()
        {

        }

        public void AddTrade(int symbolId, double value, DateTime date)
        {
            Context.Trades.Add(new Trade
            {
                SymbolId = symbolId,
                Value = value,
                Date = date
            });

            Context.SaveChanges();
        }

        public void RemoveOldTrades(DateTime endDate)
        {
            var tradesToRemove = Context.Trades.Where(t => t.Date <= endDate).ToList();
            Context.Trades.RemoveRange(tradesToRemove);
            Context.SaveChanges();
        }

        public Trade GetActualTradeForSymbol(int symbolId)
        {
            return Context.Trades.Where(t => t.SymbolId == symbolId).OrderByDescending(t => t.Date).FirstOrDefault();
        }

        public List<ActualTrade> GetActualTrades()
        {
            try
            {
                var actualTrades = from t in Context.Trades
                                   group t by t.SymbolId into grp
                                   let maxDate = grp.Max(p => p.Date)
                                   let symbolId = grp.Key
                                   from p in grp
                                   where p.Date == maxDate && p.SymbolId == symbolId && p.Symbol.Enable
                                   select new ActualTrade {
                                       Symbol = p.Symbol.Name,
                                       SymbolId = p.SymbolId,
                                       Value = p.Value,
                                       Date = DateTime.Now                                       
                                   };

                return actualTrades.ToList();
            }
            catch(Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }

            return new List<ActualTrade>();
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DB;

namespace CryptoCurrencyWebJob.CRUD
{
    public abstract class BaseCRUD
    {
        protected Context Context { get; }

        public BaseCRUD()
        {
            Context = new Context();
        }
    }
}

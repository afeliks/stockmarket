﻿using DB;
using DB.Tables;
using System.Linq;

namespace CryptoCurrencyWebJob.CRUD
{
    public class Symbols : BaseCRUD
    {

        public Symbols() : base()
        {

        }

        public void AddSymbol(string name, bool enable = true)
        {
            Context.Symbols.Add(new Symbol
            {
                Name = name,
                Enable = enable
            });

            Context.SaveChanges();
        }

        public Symbol GetSymbol(int symbolId)
        {
            return Context.Symbols.FirstOrDefault(s => s.SymbolId == symbolId);
        }

        public Symbol GetSymbolByName(string symbolName)
        {
            return Context.Symbols.FirstOrDefault(s => s.Name == symbolName);
        }

        public void EnableSymbol(int symbolId)
        {
            SetEnableOnSymbol(symbolId, true);
        }

        public void DisableSymbol(int symbolId)
        {
            SetEnableOnSymbol(symbolId, false);
        }

        private void SetEnableOnSymbol(int symbolId, bool isEnabled)
        {
            var symbol = GetSymbol(symbolId);
            if (symbol != null)
            {
                symbol.Enable = isEnabled;
                Context.SaveChanges();
            }
        }
    }
}

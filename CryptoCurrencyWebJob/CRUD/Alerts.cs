﻿using ComunicationLayer;
using DB;
using DB.Tables;
using System.Collections.Generic;
using System.Linq;

namespace CryptoCurrencyWebJob.CRUD
{
    public class Alerts : BaseCRUD
    {
        public Alerts() : base()
        {

        }

        public void AddAlert(string symbol, double value, bool aboveValue = true, bool enableLed = true, bool enableSpeaker = true)
        {
            var symbolId = Context.Symbols.Where(s => s.Name == symbol).Select(s => s.SymbolId).FirstOrDefault();

            if(symbolId <= 0)
            {
                return;
            }

            Context.Alerts.Add(new Alert
            {
                SymbolId = symbolId,
                Value = value,
                AboveValue = aboveValue,
                EnableLED = enableLed,
                EnableSpeaker = enableSpeaker
            });

            Context.SaveChanges();
        }

        public void RemoveAlert(int alertId)
        {
            var alertToRemove = Context.Alerts.FirstOrDefault(a => a.AlertId == alertId);
            if (alertToRemove != null)
            {
                Context.Alerts.Remove(alertToRemove);
                Context.SaveChanges();
            }
        }

        public List<Alert> GetAlertsForSymbol(int SymbolId)
        {
            return Context.Alerts.Where(a => a.SymbolId == SymbolId).ToList();
        }

        public Alert GetAlert(int alertId)
        {
            return Context.Alerts.FirstOrDefault(a => a.AlertId == alertId);
        }

        public List<ActualAlert> GetActualAlerts()
        {
            var trades = new Trades();
            var alerts = Context.Alerts;
            var actualAlerts = new List<ActualAlert>();

            foreach(var alert in alerts)
            {
                var actualTrade = trades.GetActualTradeForSymbol(alert.SymbolId);
                if ((alert.AboveValue && actualTrade.Value >= alert.Value) ||
                    (!alert.AboveValue && actualTrade.Value <= alert.Value))
                {
                    actualAlerts.Add(new ActualAlert
                    {
                        AlertId = alert.AlertId,
                        AboveValue = alert.AboveValue,
                        SymbolId = alert.SymbolId,
                        Value = alert.Value,
                        EnableLed = alert.EnableLED,
                        EnableSpeaker = alert.EnableSpeaker
                    });
                }
            }

            return actualAlerts;
        }

        /// <summary>
        /// Returns all saved allerts from db
        /// </summary>
        /// <returns></returns>
        public List<ActualAlert> GetAlerts()
        {
            var alerts = from a in Context.Alerts
                         select new ActualAlert
                         {
                             AboveValue = a.AboveValue,
                             AlertId = a.AlertId,
                             EnableLed = a.EnableLED,
                             EnableSpeaker = a.EnableSpeaker,
                             Symbol = a.Symbol.Name,
                             SymbolId = a.SymbolId,
                             Value = a.Value
                         };

            return alerts.ToList();
        }
    }
}

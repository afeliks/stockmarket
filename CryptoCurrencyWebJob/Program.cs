﻿using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.ServiceBus;

namespace CryptoCurrencyWebJob
{
    /*
     * If you have some problems to publish as Azure WebJob add to .csproj
     * <Import Project="$(VSToolsPath)\WebApplications\Microsoft.WebApplication.targets" Condition="'$(VSToolsPath)' != ''" />
     * https://github.com/opserver/Opserver/issues/290
     */

    // To learn more about Microsoft Azure WebJobs SDK, please see https://go.microsoft.com/fwlink/?LinkID=320976
    class Program
    {
        // Please set the following connection strings in app.config for this WebJob to run:
        // AzureWebJobsDashboard and AzureWebJobsStorage
        static void Main()
        {
            var config = new JobHostConfiguration();

            var eventHubConf = new EventHubConfiguration();
            eventHubConf.AddReceiver(Consts.EVENT_HUB_NAME, Consts.RECEIVER_CONNNECTION_STRING);
            config.UseEventHub(eventHubConf);

            var host = new JobHost(config);
            host.CallAsync(typeof(Functions).GetMethod("GetRate"));
            // The following code ensures that the WebJob will be running continuously
            host.RunAndBlock();
        }
    }
}

﻿using ExchangeSharp;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using DB;
using ExchangeSharp;

namespace CryptoCurrencyWebJob
{
    public class CryptoCurrencyRateAdapter
    {
        public List<DB.Tables.Symbol> SymbolList { get; }

        public CryptoCurrencyRateAdapter()
        {
            var context = new Context();
            SymbolList = context.Symbols.Where(s => s.Enable).ToList();
        }


        public void GetRatesAndSave()
        {
            var context = new Context();

            IExchangeAPI apiGDAX = new ExchangeGdaxAPI();

            foreach (var symbol in SymbolList)
            {
                ExchangeTicker ticker = apiGDAX.GetTicker(symbol.Name);
                ExchangeOrderBook orders = apiGDAX.GetOrderBook(symbol.Name);
                decimal askAmountSum = orders.Asks.Sum(o => o.Amount);
                decimal askPriceSum = orders.Asks.Sum(o => o.Price);
                decimal bidAmountSum = orders.Bids.Sum(o => o.Amount);
                decimal bidPriceSum = orders.Bids.Sum(o => o.Price);

                context.Trades.Add(new DB.Tables.Trade
                {
                    Date = DateTime.Now,
                    SymbolId = symbol.SymbolId,
                    Value = Convert.ToDouble(bidPriceSum)
                });
                Console.WriteLine("New trade is added");
                Console.WriteLine("{6}: {0:0.00}, {1:0.00}, {2:0.00}, {3:0.00}, {4:0.00}, {5:0.00}", ticker.Last, ticker.Volume.PriceAmount, askAmountSum, askPriceSum, bidAmountSum, bidPriceSum, symbol);
            }

            context.SaveChanges();
        }
    }
}

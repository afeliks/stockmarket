﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

//Szablon elementu Pusta strona jest udokumentowany na stronie https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x415

namespace UserInterfaceLayer
{
    /// <summary>
    /// Pusta strona, która może być używana samodzielnie lub do której można nawigować wewnątrz ramki.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public ObservableCollection<Signal> signalList;

        public MainPage()
        {
            this.InitializeComponent();
            signalList = new ObservableCollection<Signal>()
            {
                new Signal() {Symbol = "aaaa", Value = 2222},
                new Signal() {Symbol = "aaaa1", Value = 3333},
                new Signal() {Symbol = "aaaa2", Value = 4444},
                new Signal() {Symbol = "aaaa3", Value = 5555}
            };

            lvAlerts.ItemsSource = signalList;
         
        }

        private void btnAddAlarm_Click(object sender, RoutedEventArgs e)
        {
            signalList.Add(new Signal() { Symbol = "aaaa3", Value = 5555 });
        }

        private void btnRemoveAlarm_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnDisableAlarm_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}

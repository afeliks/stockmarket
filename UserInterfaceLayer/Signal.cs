﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserInterfaceLayer
{
    public class Signal
    {
        public string Symbol { get; set; }
        public double Value { get; set; }
    }
}

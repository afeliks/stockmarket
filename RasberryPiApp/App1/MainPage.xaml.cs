﻿using Microsoft.Azure.Devices.Client;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using System.Linq;
using Windows.UI.Xaml.Media;
using Windows.UI;
using Windows.Devices.Gpio;

//Szablon elementu Pusta strona jest udokumentowany na stronie https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x415

namespace App1
{
    /// <summary>
    /// Pusta strona, która może być używana samodzielnie lub do której można nawigować wewnątrz ramki.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private DeviceClient deviceClient;
        private ObservableCollection<ActualAlert> allAlertsList { get; }
        private SolidColorBrush ApplicationPageBackgroundThemeBrushColor { get; }     
        private SolidColorBrush RedColor => new SolidColorBrush(Colors.Red);
        private SolidColorBrush GreenColor => new SolidColorBrush(Colors.Green);
        public static ObservableCollection<ActualAlert> ActualAlerts = new ObservableCollection<ActualAlert>();

        public MainPage()
        {
            this.InitializeComponent();

            allAlertsList = new ObservableCollection<ActualAlert>();
            lvAlerts.ItemsSource = allAlertsList;

            ApplicationPageBackgroundThemeBrushColor = (SolidColorBrush)Application.Current.Resources["ApplicationPageBackgroundThemeBrush"];

            ActualAlerts.CollectionChanged += ActualAlerts_CollectionChanged;

            DeviceInitialize();
            RequestAllAlerts();
            ReciveMessage();
        }

        private void ActualAlerts_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (ActualAlerts.Any())
            {
                RedLed.Fill = RedColor;
                GreenLed.Fill = ApplicationPageBackgroundThemeBrushColor;
                btnDisableAlarm.IsEnabled = true;
            }
            else
            {
                RedLed.Fill = ApplicationPageBackgroundThemeBrushColor;
                GreenLed.Fill = GreenColor;
                btnDisableAlarm.IsEnabled = false;
            }

            lvAlerts.ItemsSource = null; //force refresh
            lvAlerts.ItemsSource = allAlertsList;            
        }

        private void DeviceInitialize()
        {
            const string HOST_NAME = "",
                         DEVICE_ID = "",
                         DEVICE_KEY = "";

            deviceClient = DeviceClient.Create(HOST_NAME,
                           AuthenticationMethodFactory.CreateAuthenticationWithRegistrySymmetricKey(DEVICE_ID, DEVICE_KEY),
                           TransportType.Http1);
        }

        private async void RequestAllAlerts()
        {
            Message message = new Message();
            message.ContentType = "sendMeAlerts";
            await deviceClient.SendEventAsync(message);
        }

        private async void ReciveMessage()
        {           
            while (true)
            {
                Message message = await deviceClient.ReceiveAsync();
                if (message == null)
                    continue;

                if (message.Properties["type"] == "alert") //allerts occur
                {
                    List<ActualAlert> alerts = ConvertToArray<ActualAlert>(message.BodyStream);
                    ActualAlerts.Clear();
                    alerts.ForEach(a => ActualAlerts.Add(a));
                }
                else if (message.Properties["type"] == "allAlert")
                {
                    List<ActualAlert> allAlerts = ConvertToArray<ActualAlert>(message.BodyStream);
                    allAlertsList.Clear();
                    allAlerts.ForEach(a => allAlertsList.Add(a));
                }
                else if (message.Properties["type"] == "trade")
                {
                    List<ActualTrade> trades = ConvertToArray<ActualTrade>(message.BodyStream);
                    lvTrades.ItemsSource = trades;                  
                }

                deviceClient.CompleteAsync(message); //is faster call this method async                
            }
        }

        private List<T> ConvertToArray<T>(Stream stream)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(List<T>));
            return (List<T>)serializer.Deserialize(stream);
        }

        /// <summary>
        /// Converts object to byte array
        /// </summary>
        private static byte[] ConvertToByteArray<T>(List<T> list)
        {
            var serializer = new XmlSerializer(typeof(List<T>));
            var mStream = new MemoryStream();
            serializer.Serialize(mStream, list);
            return mStream.ToArray();
        }


        private async void btnAddAlarm_ClickAsync(object sender, RoutedEventArgs e)
        {
            AddAlarmDialog dialog = new AddAlarmDialog();
            await dialog.ShowAsync();

            if (dialog.SignalToAdd != null)
            {
                var byteArray = ConvertToByteArray(new List<ActualAlert> { dialog.SignalToAdd });
                Message message = new Message(byteArray);
                message.ContentType = "alerttoAdd";
                await deviceClient.SendEventAsync(message);
            }
        }

        private async void btnRemoveAlarm_Click(object sender, RoutedEventArgs e)
        {
            if (lvAlerts.SelectedItems == null)
                return;

            List<ActualAlert> alertsToRemove = lvAlerts.SelectedItems.Cast<ActualAlert>().ToList();

            var convertedAlertsToRemove = ConvertToByteArray<ActualAlert>(alertsToRemove);

            var message = new Message(convertedAlertsToRemove);
            message.Properties.Add("type", "alertToRemove");
            message.ContentType = "alertToRemove";
            await deviceClient.SendEventAsync(message); // Async !!!

        }

        private async void btnDisableAlarm_Click(object sender, RoutedEventArgs e)
        {
            var convertedAlertsToRemove = ConvertToByteArray(ActualAlerts.ToList());

            var message = new Message(convertedAlertsToRemove);
            message.Properties.Add("type", "alertToRemove");
            message.ContentType = "alertToRemove";
            await deviceClient.SendEventAsync(message);
        }

        private void lvAlerts_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(lvAlerts.SelectedItems.Any())
            {
                btnRemoveAlarm.IsEnabled = true;
            }
            else
            {
                btnRemoveAlarm.IsEnabled = false;
            }
        }
    }
}

﻿namespace App1
{
    public class Signal
    {
        public string Symbol { get; set; }
        public double Value { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media;

namespace App1
{
    public class ColorConverter  : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            int alertId = (int)value;
            if(MainPage.ActualAlerts.Any(a => a.AlertId == alertId))
            {
                return new SolidColorBrush(Colors.Red);
            }

            return new SolidColorBrush(Colors.Transparent);
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return new object();
        }
    }
}

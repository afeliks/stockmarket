﻿using System;

namespace App1
{
    /// <summary>
    /// This class is using to send actual trade for given symbol from cloud to rasberry PI
    /// </summary>

    public class ActualTrade
    {
        public string Symbol { get; set; }
        public int SymbolId { get; set; }
        public double Value { get; set; }
        public DateTime Date { get; set; }
    }
}

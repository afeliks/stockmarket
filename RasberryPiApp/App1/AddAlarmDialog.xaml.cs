﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

//Szablon elementu Okno dialogowe zawartości jest udokumentowany na stronie https://go.microsoft.com/fwlink/?LinkId=234238

namespace App1
{
    public sealed partial class AddAlarmDialog : ContentDialog
    {
        public ActualAlert SignalToAdd { get; set; }

        public AddAlarmDialog()
        {
            this.InitializeComponent();
        }

        private void btn1_Click(object sender, RoutedEventArgs e)
        {
            SignalToAdd = new ActualAlert()
            {
                Symbol = txtSymbol.Text,
                Value = double.Parse(txtValue.Text),
                AboveValue = radioAbove.IsChecked ?? false,
                EnableSpeaker = speakerOn.IsChecked ?? false,
                EnableLed = ledOn.IsChecked ?? true
            };

            this.Hide();
        }

        private void btn2_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
        }
    }
}

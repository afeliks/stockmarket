﻿using DB.Tables;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB
{
    public class Context : DbContext
    {
        public Context() : base("mysqlconn")
        {

        }

        public DbSet<Symbol> Symbols { get; set; }
        public DbSet<Alert> Alerts { get; set; }
        public DbSet<Trade> Trades { get; set; }
    }
}

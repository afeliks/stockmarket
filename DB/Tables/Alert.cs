﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.Tables
{
    [Table("Alert")]
    public class Alert
    {
        public int AlertId { get; set; }
        public int SymbolId { get; set; }
        public Symbol Symbol { get; set; }
        public double Value { get; set; }
        public bool AboveValue { get; set; }
        public bool EnableLED { get; set; }
        public bool EnableSpeaker { get; set; }
    }
}

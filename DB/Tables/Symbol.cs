﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.Tables
{
    [Table("Symbol")]
    public class Symbol
    {
        public int SymbolId { get; set; }
        public string Name { get; set; }
        public bool Enable { get; set; }
        public ICollection<Trade> Trades { get; set; }
        public ICollection<Alert> Alerts { get; set; }
    }
}
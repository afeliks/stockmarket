﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.Tables
{
    [Table("Trade")]
    public class Trade
    {
        public int TradeId { get; set; }
        public int SymbolId { get; set; }
        public Symbol Symbol { get; set; }
        public double Value { get; set; }
        public DateTime Date { get; set; }
    }
}
